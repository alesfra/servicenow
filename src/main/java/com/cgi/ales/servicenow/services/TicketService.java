package com.cgi.ales.servicenow.services;

import com.cgi.ales.servicenow.dtos.TicketDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TicketService {
    TicketDto createNewTicket(TicketDto ticketToCreate);
    TicketDto getTicketByName(String name);
    TicketDto getTicketById(Long id);
    Page findAllTickets(Pageable pageable);
}
