package com.cgi.ales.servicenow.services;

import com.cgi.ales.servicenow.dtos.TicketDto;
import com.cgi.ales.servicenow.entities.Ticket;
import com.cgi.ales.servicenow.mappers.BeanMapper;
import com.cgi.ales.servicenow.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {

    private TicketRepository ticketRepository;
    private BeanMapper beanMapper;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository, BeanMapper beanMapper) {
        this.ticketRepository = ticketRepository;
        this.beanMapper = beanMapper;
    }

    public TicketDto createNewTicket(TicketDto ticketToCreate) {
        Ticket ticket = beanMapper.mapTo(ticketToCreate, Ticket.class);
        Ticket savedTicket = ticketRepository.save(ticket);

        return beanMapper.mapTo(savedTicket, TicketDto.class);
    }

    public TicketDto getTicketById(Long id) {
        return beanMapper.mapTo(ticketRepository.getTicketById(id), TicketDto.class);
    }

    public TicketDto getTicketByName(String name) {
        return beanMapper.mapTo(ticketRepository.getTicketByName(name), TicketDto.class);
    }

    public Page<TicketDto> findAllTickets(Pageable pageable) {
        Page<Ticket> ticketPage = ticketRepository.findAll(pageable);
        return beanMapper.mapTo(ticketPage, TicketDto.class);
    }
}