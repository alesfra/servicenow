package com.cgi.ales.servicenow.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "tickets")
@ApiModel(description = "Details about ticket structure")
public class Ticket implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated ticket ID")
    private Long id;

    @Column(nullable = false, name = "name")
    @ApiModelProperty(notes = "Ticket name")
    private String name;

    @Column(nullable = false, name = "email")
    @ApiModelProperty(notes = "Email of ticket creator")
    private String email;

    @Column(nullable = false, name = "id_person_creator")
    @ApiModelProperty(notes = "Id of person that created a ticket")
    private Long idPersonCreator;

    @Column(nullable = false, name = "id_person_assigned")
    @ApiModelProperty(notes = "Id of person that will be assigned to the particular ticket")
    private Long idPersonAssigned;

    @Column(nullable = true, name = "creation_date_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(notes = "Time of ticket creation")
    private LocalDateTime creationDateTime = LocalDateTime.now().withNano(0);

    public Ticket() {
    }

    public Ticket(Long id, String name, String email, Long idPersonCreator,
                  Long idPersonAssigned, LocalDateTime creationDateTime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = creationDateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
}