package com.cgi.ales.servicenow.repositories;

import com.cgi.ales.servicenow.entities.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    Ticket getTicketById(Long id);
    Ticket getTicketByName(String name);
}
