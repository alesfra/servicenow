package com.cgi.ales.servicenow.controllers;

import com.cgi.ales.servicenow.dtos.TicketDto;
import com.cgi.ales.servicenow.services.TicketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value = "/tickets")
@RestController
@RequestMapping(path = "/tickets")
public class TicketController {

    private TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @ApiOperation(
            value = "Insert a new ticket",
            httpMethod = "POST",
            response = void.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data successfully inserted"),
            @ApiResponse(code = 401, message = "You are not authorized to insert any data"),
            @ApiResponse(code = 403, message = "Forbidden")
    })
    @PostMapping(
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )
    public void createNewTicket(@RequestBody TicketDto newTicket) throws Exception {
        ticketService.createNewTicket(newTicket);
    }

    @ApiOperation(
            value = "Get ticket by id",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "GET",
            response = TicketDto.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data successfully retrieved"),
            @ApiResponse(code = 401, message = "You are not authorized to view the data"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    @GetMapping(path = "/{id}")
    public ResponseEntity<TicketDto> getTicket(@PathVariable("id") Long id) {
        return new ResponseEntity<>(ticketService.getTicketById(id), HttpStatus.OK);
    }


    @ApiOperation(
            value = "Get ticket by it's name",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "GET",
            response = TicketDto.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data successfully retrieved"),
            @ApiResponse(code = 401, message = "You are not authorized to view the data"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    @GetMapping(path = "/particular-ticket")
    public ResponseEntity<TicketDto> getTicket(@RequestParam("name") String name) {
        return new ResponseEntity<>(ticketService.getTicketByName(name), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get all tickets paginated",
            httpMethod = "GET",
            response = TicketDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data successfully retrieved"),
            @ApiResponse(code = 401, message = "You are not authorized to view the data"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    @GetMapping
    public ResponseEntity<Page<TicketDto>> getAllTicketsPaginated(Pageable pageable) {
        return ResponseEntity.ok(ticketService.findAllTickets(pageable));
    }
}